function inputClick(clickedId) {
  console.log('clicked_id: ' + clickedId);

  _toggleClass(clickedId, 'active');

  if(_isClassExists('and-p', 'active') && _isClassExists('and-q', 'active')) {
    _addClass('and-out', 'active');
  } else {
    _removeClass('and-out', 'active');
  }
  
  if(_isClassExists('not-p', 'active')) {
	 _removeClass('not-out', 'active');
    
  } else {
   _addClass('not-out', 'active');
  }
  
    if(!_isClassExists('or-p', 'active') && !_isClassExists('or-q', 'active')) {
    _removeClass('or-out', 'active');
  } else {
    
	_addClass('or-out', 'active');
  }
  if((_isClassExists('xor-p', 'active') && _isClassExists('xor-q', 'active')) || (!_isClassExists('xor-p', 'active') && !_isClassExists('xor-q', 'active'))) {
    
	_removeClass('xor-out', 'active');
  } 

  else {
    _addClass('xor-out', 'active');
  }

  _clearBackgroundsInTable();

  var idTable = '';
  idTable += _isClassExists('and-p', 'active') ? '1' : '0';
  idTable += _isClassExists('and-q', 'active') ? '1' : '0';
  _addClass(idTable, 'active');
  
  
   
  var idTable2 = '';
  idTable2 += _isClassExists('not-p', 'active') ? 'a' : 'b';
  idTable2 += _isClassExists('not-p', 'display') ? 'b' : 'a';
  _addClass(idTable2, 'active');
  
    var idTable3 = '';
  idTable3 += _isClassExists('or-p', 'active') ? 'c' : 'd';
  idTable3 += _isClassExists('or-q', 'active') ? 'c' : 'd';
  _addClass(idTable3, 'active');
 


  var idTable4 = '';
  idTable4 += _isClassExists('xor-p', 'active') ? 'f' : 'g';
  idTable4 += _isClassExists('xor-q', 'active') ? 'f' : 'g';
  _addClass(idTable4, 'active');

}
function _clearBackgroundsInTable() {
  var el = document.getElementsByTagName('tr');
      
  for (var i = 0; i < el.length; i++) {
    el[i].classList.remove('active');
  }
}

function _toggleClass(id, className) {
  document.getElementById(id).classList.toggle(className);
}

function _addClass(id, className) {
  document.getElementById(id).classList.add(className);
}

function _removeClass(id, className) {
  document.getElementById(id).classList.remove(className);
}

function _isClassExists(id, className) {
  return document.getElementById(id).classList.contains(className);
}
